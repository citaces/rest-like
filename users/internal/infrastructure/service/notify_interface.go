package service

import (
	"gitlab.com/citaces/rest-like/users/config"
	"gitlab.com/citaces/rest-like/users/internal/infrastructure/errors"
	"gitlab.com/citaces/rest-like/users/internal/provider"
	"go.uber.org/zap"
)

type Notifier interface {
	Push(in PushIn) PushOut
}

type Notify struct {
	conf   config.Email
	email  provider.Sender
	logger *zap.Logger
}

func NewNotify(conf config.Email, email provider.Sender, logger *zap.Logger) Notifier {
	return &Notify{email: email, conf: conf, logger: logger}
}

func (n *Notify) Push(in PushIn) PushOut {
	err := n.email.Send(provider.SendIn{
		To:    in.Identifier,
		From:  n.conf.From,
		Title: in.Title,
		Type:  provider.TextPlain,
		Data:  in.Data,
	})
	if err != nil {
		n.logger.Error("send email err", zap.Error(err))
		return PushOut{
			ErrorCode: errors.NotifyEmailSendErr,
		}
	}

	return PushOut{}
}

const (
	PushEmail = iota + 1
	PushPhone
)

type PushIn struct {
	Identifier string
	Type       int
	Title      string
	Data       []byte
	Options    []interface{}
}

type PushOut struct {
	ErrorCode int
}
