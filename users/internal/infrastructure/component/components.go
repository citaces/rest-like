package component

import (
	"github.com/ptflp/godecoder"
	"gitlab.com/citaces/rest-like/users/config"
	"gitlab.com/citaces/rest-like/users/internal/infrastructure/responder"
	"gitlab.com/citaces/rest-like/users/internal/infrastructure/service"
	"gitlab.com/citaces/rest-like/users/internal/infrastructure/tools/cryptography"
	"go.uber.org/zap"
)

type Components struct {
	Conf         config.AppConf
	Notify       service.Notifier
	TokenManager cryptography.TokenManager
	Responder    responder.Responder
	Decoder      godecoder.Decoder
	Logger       *zap.Logger
	Hash         cryptography.Hasher
}

func NewComponents(conf config.AppConf, notify service.Notifier, tokenManager cryptography.TokenManager, responder responder.Responder, decoder godecoder.Decoder, hash cryptography.Hasher, logger *zap.Logger) *Components {
	return &Components{Conf: conf, Notify: notify, TokenManager: tokenManager, Responder: responder, Decoder: decoder, Hash: hash, Logger: logger}
}
