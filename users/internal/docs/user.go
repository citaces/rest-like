package docs

import ucontroller "gitlab.com/citaces/rest-like/users/internal/modules/user/controller"

// swagger:route GET /api/1/user/profile user profileRequest
// Информация о текущем пользователе.
// security:
//  - Bearer: []
// responses:
//   200: profileResponse

// swagger:response profileResponse
type profileResponse struct {
	// in:body
	Body ucontroller.ProfileResponse
}
