package controller

import (
	"log"
	"net/http"
	"strconv"

	"github.com/go-chi/chi/v5"
	"github.com/ptflp/godecoder"
	"gitlab.com/citaces/rest-like/users/internal/infrastructure/component"
	"gitlab.com/citaces/rest-like/users/internal/infrastructure/errors"
	"gitlab.com/citaces/rest-like/users/internal/infrastructure/responder"
	"gitlab.com/citaces/rest-like/users/internal/modules/user/service"
)

type Userer interface {
	Profile(http.ResponseWriter, *http.Request)
	ChangePassword(http.ResponseWriter, *http.Request)
}

type User struct {
	service service.Userer
	responder.Responder
	godecoder.Decoder
}

func NewUser(service service.Userer, components *component.Components) Userer {
	return &User{service: service, Responder: components.Responder, Decoder: components.Decoder}
}

func (u *User) Profile(w http.ResponseWriter, r *http.Request) {
	userID := chi.URLParam(r, "userID")
	id, err := strconv.Atoi(userID)
	if err != nil {
		log.Fatal("Error when trying to convert the value of the user ID to an integer.")
		return
	}
	out := u.service.GetByID(r.Context(), service.GetByIDIn{UserID: id})

	if out.ErrorCode != errors.NoError {
		u.OutputJSON(w, ProfileResponse{
			ErrorCode: out.ErrorCode,
			Data: Data{
				Message: "retrieving user error",
			},
		})
		return
	}

	u.OutputJSON(w, ProfileResponse{
		Success:   true,
		ErrorCode: out.ErrorCode,
		Data: Data{
			User: *out.User,
		},
	})
}

func (u *User) ChangePassword(w http.ResponseWriter, r *http.Request) {

	userID := chi.URLParam(r, "userID")
	newPassword := chi.URLParam(r, "new")
	id, err := strconv.Atoi(userID)
	if err != nil {
		http.Error(w, "Error when trying to convert the value of the user ID to an integer.", http.StatusInternalServerError)
		return
	}
	result := u.service.ChangePassword(r.Context(), service.ChangePasswordIn{
		UserID:      id,
		NewPassword: newPassword,
	})
	u.OutputJSON(w, service.ChangePasswordOut{
		Success:   result.Success,
		ErrorCode: result.ErrorCode,
	})
}
