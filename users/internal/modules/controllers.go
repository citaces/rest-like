package modules

import (
	"gitlab.com/citaces/rest-like/users/internal/infrastructure/component"
	acontroller "gitlab.com/citaces/rest-like/users/internal/modules/auth/controller"
	ucontroller "gitlab.com/citaces/rest-like/users/internal/modules/user/controller"
)

type Controllers struct {
	Auth acontroller.Auther
	User ucontroller.Userer
}

func NewControllers(services *Services, components *component.Components) *Controllers {
	authController := acontroller.NewAuth(services.Auth, components)
	userController := ucontroller.NewUser(services.User, components)

	return &Controllers{
		Auth: authController,
		User: userController,
	}
}
