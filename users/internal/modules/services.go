package modules

import (
	"gitlab.com/citaces/rest-like/users/internal/infrastructure/component"
	aservice "gitlab.com/citaces/rest-like/users/internal/modules/auth/service"
	uservice "gitlab.com/citaces/rest-like/users/internal/modules/user/service"
	"gitlab.com/citaces/rest-like/users/internal/storages"
)

type Services struct {
	User uservice.Userer
	Auth aservice.Auther
}

func NewServices(storages *storages.Storages, components *component.Components) *Services {
	userService := uservice.NewUserService(storages.User, components.Logger)
	return &Services{
		User: userService,
		Auth: aservice.NewAuth(userService, storages.Verify, components),
	}
}
