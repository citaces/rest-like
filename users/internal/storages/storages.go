package storages

import (
	"gitlab.com/citaces/rest-like/users/internal/db/adapter"
	"gitlab.com/citaces/rest-like/users/internal/infrastructure/cache"
	vstorage "gitlab.com/citaces/rest-like/users/internal/modules/auth/storage"
	ustorage "gitlab.com/citaces/rest-like/users/internal/modules/user/storage"
)

type Storages struct {
	User   ustorage.Userer
	Verify vstorage.Verifier
}

func NewStorages(sqlAdapter *adapter.SQLAdapter, cache cache.Cache) *Storages {
	return &Storages{
		User:   ustorage.NewUserStorage(sqlAdapter, cache),
		Verify: vstorage.NewEmailVerify(sqlAdapter),
	}
}
