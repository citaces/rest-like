package service

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/lib/pq"
	"gitlab.com/citaces/rest-like/internal/infrastructure/errors"
	"gitlab.com/citaces/rest-like/internal/models"
	"gitlab.com/citaces/rest-like/internal/modules/user/storage"
	"go.uber.org/zap"
)

type ProfileResponse struct {
	Success   bool `json:"success"`
	ErrorCode int  `json:"error_code,omitempty"`
	Data      Data `json:"data"`
}

type Data struct {
	Message string      `json:"message,omitempty"`
	User    models.User `json:"user,omitempty"`
}

type UserService struct {
	storage storage.Userer
	logger  *zap.Logger
}

func NewUserService(storage storage.Userer, logger *zap.Logger) *UserService {
	return &UserService{storage: storage, logger: logger}
}

func (u *UserService) Create(ctx context.Context, in UserCreateIn) UserCreateOut {
	var dto models.UserDTO
	dto.SetName(in.Name).
		SetPhone(in.Phone).
		SetEmail(in.Email).
		SetPassword(in.Password).
		SetRole(in.Role)

	userID, err := u.storage.Create(ctx, dto)
	if err != nil {
		if v, ok := err.(*pq.Error); ok && v.Code == "23505" {
			return UserCreateOut{
				ErrorCode: errors.UserServiceUserAlreadyExists,
			}
		}
		return UserCreateOut{
			ErrorCode: errors.UserServiceCreateUserErr,
		}
	}

	return UserCreateOut{
		UserID: userID,
	}
}

func (u *UserService) VerifyEmail(ctx context.Context, in UserVerifyEmailIn) UserUpdateOut {
	dto, err := u.storage.GetByID(ctx, in.UserID)
	if err != nil {
		u.logger.Error("user: GetByEmail err", zap.Error(err))
		return UserUpdateOut{
			ErrorCode: errors.UserServiceRetrieveUserErr,
		}
	}
	dto.SetEmailVerified(true)
	err = u.storage.Update(ctx, dto)
	if err != nil {
		u.logger.Error("user: update err", zap.Error(err))
		return UserUpdateOut{
			ErrorCode: errors.UserServiceUpdateErr,
		}
	}

	return UserUpdateOut{
		Success: true,
	}
}

func (u *UserService) ChangePassword(ctx context.Context, in ChangePasswordIn) ChangePasswordOut {
	url := fmt.Sprintf("http://usersapi:8081/api/1/user/reset/%v/%v", in.UserID, in.NewPassword)
	req, err := http.NewRequestWithContext(ctx, "POST", url, nil)
	if err != nil {
		u.logger.Error("user:change password err req")
		return ChangePasswordOut{
			ErrorCode: errors.AuthServiceWrongPasswordErr,
		}
	}
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		u.logger.Error("user:change password err resp")
		return ChangePasswordOut{
			ErrorCode: errors.UserServiceUpdateErr,
		}
	}
	var chp ChangePasswordOut
	err = json.NewDecoder(resp.Body).Decode(&chp)
	if err != nil {
		u.logger.Error("user:change password err decode")
		return ChangePasswordOut{
			ErrorCode: errors.UserServiceUpdateErr,
		}
	}
	return chp
}

func (u *UserService) GetByEmail(ctx context.Context, in GetByEmailIn) UserOut {
	userDTO, err := u.storage.GetByEmail(ctx, in.Email)
	if err != nil {
		u.logger.Error("user: GetByEmail err", zap.Error(err))
		return UserOut{
			ErrorCode: errors.UserServiceRetrieveUserErr,
		}
	}

	return UserOut{
		User: &models.User{
			ID:            userDTO.GetID(),
			Name:          userDTO.GetName(),
			Phone:         userDTO.GetPhone(),
			Email:         userDTO.GetEmail(),
			Password:      userDTO.GetPassword(),
			Role:          userDTO.GetRole(),
			Verified:      userDTO.Verified,
			EmailVerified: userDTO.EmailVerified,
			PhoneVerified: userDTO.PhoneVerified,
		},
	}
}

func (u *UserService) GetByID(ctx context.Context, in GetByIDIn) UserOut {
	url := fmt.Sprintf("http://usersapi:8081/api/1/user/profile/%v", in.UserID)
	req, err := http.NewRequestWithContext(ctx, "GET", url, nil)
	if err != nil {
		u.logger.Error("user: GetByID err req", zap.Error(err))
		return UserOut{
			ErrorCode: errors.UserServiceRetrieveUserErr,
		}
	}
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		u.logger.Error("user: GetByID err resp", zap.Error(err))
		return UserOut{
			ErrorCode: errors.UserServiceRetrieveUserErr,
		}
	}
	if resp.StatusCode != http.StatusOK {
		u.logger.Error("user: GetByID err statusCode", zap.Error(err))
		return UserOut{
			ErrorCode: errors.UserServiceRetrieveUserErr,
		}
	}
	var profileResponse ProfileResponse
	err = json.NewDecoder(resp.Body).Decode(&profileResponse)
	if err != nil {
		u.logger.Error("user: GetByID err Decode", zap.Error(err))
		return UserOut{
			ErrorCode: errors.UserServiceRetrieveUserErr,
		}
	}
	return UserOut{User: &profileResponse.Data.User}
}
