package controller

import (
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/ptflp/godecoder"
	"gitlab.com/citaces/rest-like/internal/infrastructure/component"
	"gitlab.com/citaces/rest-like/internal/infrastructure/errors"
	"gitlab.com/citaces/rest-like/internal/infrastructure/handlers"
	"gitlab.com/citaces/rest-like/internal/infrastructure/responder"
	"gitlab.com/citaces/rest-like/internal/infrastructure/tools/cryptography"
	"gitlab.com/citaces/rest-like/internal/modules/user/service"
)

type Userer interface {
	Profile(http.ResponseWriter, *http.Request)
	ChangePassword(http.ResponseWriter, *http.Request)
}

type User struct {
	service service.Userer
	responder.Responder
	godecoder.Decoder
}

func NewUser(service service.Userer, components *component.Components) Userer {
	return &User{service: service, Responder: components.Responder, Decoder: components.Decoder}
}

func (u *User) Profile(w http.ResponseWriter, r *http.Request) {
	claims, err := handlers.ExtractUser(r)
	if err != nil {
		u.ErrorBadRequest(w, err)
		return
	}
	out := u.service.GetByID(r.Context(), service.GetByIDIn{UserID: claims.ID})
	if out.ErrorCode != errors.NoError {
		u.OutputJSON(w, ProfileResponse{
			ErrorCode: out.ErrorCode,
			Data: Data{
				Message: "retrieving user error",
			},
		})
		return
	}

	u.OutputJSON(w, ProfileResponse{
		Success:   true,
		ErrorCode: out.ErrorCode,
		Data: Data{
			User: *out.User,
		},
	})
}

func (u *User) ChangePassword(w http.ResponseWriter, r *http.Request) {
	claims, err := handlers.ExtractUser(r)
	if err != nil {
		u.ErrorBadRequest(w, err)
	}
	out := u.service.GetByID(r.Context(), service.GetByIDIn{UserID: claims.ID})
	oldPassword := chi.URLParam(r, "old")
	newPassword := chi.URLParam(r, "new")
	hashedPassword := out.User.Password

	if !cryptography.CheckPassword(hashedPassword, oldPassword) {
		http.Error(w, "invalid old password", http.StatusInternalServerError)
		return
	}

	result := u.service.ChangePassword(r.Context(), service.ChangePasswordIn{
		UserID:      out.User.ID,
		OldPassword: oldPassword,
		NewPassword: newPassword,
	})
	u.OutputJSON(w, service.ChangePasswordOut{
		Success:   result.Success,
		ErrorCode: result.ErrorCode,
	})
}
