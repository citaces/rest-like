package docs

import (
	ucontroller "gitlab.com/citaces/rest-like/internal/modules/user/controller"
	"gitlab.com/citaces/rest-like/internal/modules/user/service"
)

// swagger:route GET /api/1/user/profile user profileRequest
// Информация о текущем пользователе.
// security:
//  - Bearer: []
// responses:
//   200: profileResponse

// swagger:response profileResponse
//
//nolint:all
type profileResponse struct {
	// in:body
	Body ucontroller.ProfileResponse
}

// swagger:route POST /api/1/user/reset/{old}/{new} user resetRequest
// Сменить пароль.
// security:
//  - Bearer: []
// responses:
//   200: resetResponse

// swagger:parameters resetRequest
//
//nolint:all
type resetRequest struct {
	// Старый пароль
	//
	// required: true
	//in:path
	OldPassword string `json:"old"`

	// Новый пароль
	//
	// required: true
	//in:path
	NewPassword string `json:"new"`
}

// swagger:response resetResponse
//
//nolint:all
type resetResponse struct {
	// in:body
	Status service.ChangePasswordOut
}
