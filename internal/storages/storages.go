package storages

import (
	"gitlab.com/citaces/rest-like/internal/db/adapter"
	"gitlab.com/citaces/rest-like/internal/infrastructure/cache"
	vstorage "gitlab.com/citaces/rest-like/internal/modules/auth/storage"
	ustorage "gitlab.com/citaces/rest-like/internal/modules/user/storage"
)

type Storages struct {
	User   ustorage.Userer
	Verify vstorage.Verifier
}

func NewStorages(sqlAdapter *adapter.SQLAdapter, cache cache.Cache) *Storages {
	return &Storages{
		User:   ustorage.NewUserStorage(sqlAdapter, cache),
		Verify: vstorage.NewEmailVerify(sqlAdapter),
	}
}
